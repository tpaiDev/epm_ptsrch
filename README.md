# Állatkereső web application #

----

## Web application for lost pets. You can create statements about your missed pet, and from the other side people can alert the owners if they found the pet. ##

----

**Used languages:** 1. HTML + CSS 2. JAVASCRIPT 3. PHP 4. SQL

**Used software:** 1. Notepad++ v7.5.6 (programming) 2. WAMP (almost every part of) 3. Total Commander (FTP) 4. The Regex Coach (to make regular expressions) 5. Pickpick (to pick colors to work) 6. Bitbucket + Sourcetree (git)

**Tested browseres:** 1. Google Chrome (Max resolution & default mobile resolution). 2. Mozilla Firefox (Max resolution)

----


*Created for Epam summer intenship competition, Epam 2018 - Created by tkSrchD (Tápai Dávid).*