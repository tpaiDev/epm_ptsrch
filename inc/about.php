<?php 
	if(!defined("WEB_INDEX")) {
		die("Hib&#225;s hivatkoz&#225;s.");
	}
?>
<i style="color: #666;">Az oldalról</i>
<hr style="margin: 5px 0px;"></hr>
<p style="font-size: 18px;">A háziállattartás nagy felelőséggel jár, de még így is felmerülhetnek váratlan problémák amellyel az állattartók nem számolnak. Ilyen például az amikor a gazda akarata ellenére, kis kedvence eltűnik. Ez történhet akár figyelmetlenségből, de akár olyan behatásoktól is ami a gazda akarata és figyelme ellenére megtörténik. Ilyen akár csak a szilveszter. A szilveszter csodálatos dolog. Az emberek jól érzik magukat, boldogan köszöntik az újévet. De sajnos néhányan nem gondolnak a kisállatokra. A tűzijátékok jó dolgoknak tűnek, de nem az állatoknak. Ilyenkor ugyanis az elveszett/elszökött macskák és kutyák aránya sokkal magasabb. Lehet a gazdi felkészült, a probléma így is sokszor előfordul. </p>
<br/>
<p style="font-size: 18px;"> Éppen ezért lett létrehozta ez az oldal. Ha bár a jószágok megszökési kényszerét nem tudja megakadályozni, annak megtalálását nagyon is elősegíti. A rendszer működése nagyon egyszerű. A gazda jelenti kis kedvence eltűnését, az esetleges megtaláló pedig az oldalon a kutya fajtájára/ismertetőjére vagy megtalálási helyére keresve értesítheti a gazdit. Ha az állat nincs regisztrálva az oldalra 'megtalált kisállat'-ként is lehet regisztrálni. Ebben az esettben az oldal működése fordított.</p>
<br/>
<i>Az oldal a 2018-as Epam Nyári Gyakorlatára lett létrehozva, és jelenleg annak a keretein belül működik. Az Állatkereső nyílt forráskódú, mely a következő linken érhető el: <a href="https://bitbucket.org/tkSrchD/epm_ptsrch/overview">Bitbucket</a></i>