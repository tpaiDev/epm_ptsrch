<?php 
	if(!defined("WEB_INDEX")) {
		die("Hib&#225;s hivatkoz&#225;s.");
	}
?>
<i style="color: #666;">Főoldal</i>
<hr style="margin: 5px 0px;"></hr>
<h2>Elvesztett háziállata? Nincs minden veszve! Az ingyenes és regisztráció mentes közösségen alapuló szolgáltatásunkban, bejelentést hozhat létre, vagy kereshet a már megtalált kisállatok adatbázisában.</h2>
<br/>
<h2>Egy kóbor állatra talált és szeretné megtalálni a gazdáját? Ellenőrizze, hogy lett-e már létrehozva keresési bejelentés, ha nem tudassa azt azzal, hogy létrehoz egy megtalálási jelentést. </h2>
<br/>
<h2>Irány az <a href="index.php?s=search" style="color: #20B3CE;">Állatkereső</a>.</h2>
