<?php 
	if(!defined("WEB_INDEX")) {
		die("Hib&#225;s hivatkoz&#225;s.");
	}
?>
<?php
	date_default_timezone_set("Europe/Budapest"); 
	
	$pet_species = "";
	$pet_variety = "";
	$pet_city = "";
	$pet_date = "";
	$pet_characterization = "";
	
	// Vars
	if(isset($_POST["submit"])) {
		$pet_species = $_POST["species"];
		$pet_variety = $_POST["variety"];
		$pet_city = $_POST["city"];
		$pet_date = $_POST["date"];
		$pet_characterization = $_POST["characterization"];
		$pet_third_choose = $_POST["third_choose"];
		$user_name = $_POST["name"];
		$user_mobile = $_POST["mobile"];
		$user_mail = $_POST["mail"];
		
		$data_error = false;
		$log_error = "";
			
			
		// Regex
		$mail_regex = '/^[A-z0-9._+-]+@[a-z0-9.-_]+\.[a-z]{2,}$/';
		
		// against SQL injection
		$pet_species = strip_tags($pet_species);
		$pet_variety = strip_tags($pet_variety);
		$pet_city = strip_tags($pet_city);
		$pet_characterization = strip_tags($pet_characterization);
		$user_name = strip_tags($user_name);
		$user_mail = strip_tags($user_mail);
		
		// Validate
		
		if (!$pet_species) {
			$data_error = true;
			$log_error = $log_error . "<br/> A faj megadása kötelező!";
		}
		
		if (!$pet_variety) {
			$data_error = true;
			$log_error = $log_error . "<br/> A fajta megadása kötelező!";
		}
		
		if (!$pet_city) {
			$data_error = true;
			$log_error = $log_error . "<br/> Az eltűnés/megtalálás városának megadása kötelező!";
		}
		
		if (!$pet_characterization) {
			$data_error = true;
			$log_error = $log_error . "<br/> A leírás megadása kötelező!";
		}elseif (strlen($pet_characterization) < 20) {
			$data_error = true;
			$log_error = $log_error . "<br/> A leírásnak minimum 20 karakter hosszúságúnak kell lennie!";
		}
		
		if($pet_third_choose ==1 || $pet_third_choose == 2) {
			
		}else {
			$data_error = true;
			$log_error = $log_error . "<br/>A 3. pontban foglaltaknak megfelelően, azt kötelező meagadni!";
		}
		
		if (!$user_name) {
			$data_error = true;
			$log_error = $log_error . "<br/> A név megadása kötelező!";
		}
		
		if (!$user_mobile) {

		}elseif (strlen($user_mobile) > 11 || strlen($user_mobile) < 9) {
			$data_error = true;
			$log_error = $log_error . "<br/> Érvénytelen telefonszám!";
		}else {
				
		}
		
		if (!$user_mail) {
			$data_error = true;
			$log_error = $log_error . "<br/> Az e-mail cím megadása kötelező!";
			}elseif (strlen($user_mail) < 6) {
			$data_error = true;
			$log_error = $log_error . "<br/> Az e-mail cím nem lehet rövidebb 6 karakternél!";
			}elseif (strlen($user_mail) > 250) {
			$data_error = true;
			$log_error = $log_error . "<br/> Az e-mail cím nem lehet hosszabb 250 karakternél!!";
			}elseif (!preg_match($mail_regex, $user_mail)) {
			$data_error = true;
			$log_error = $log_error . "<br/> Az e-mail cím nem megfelelő formátumú!";
			}

			$date_now = date("Y-m-d");

			if(!$pet_date) {
				
			}else {
			if ($date_now >= $pet_date) {
					
				}else{
					$data_error = true;
					$log_error = $log_error . "<br/> A megadott dátum nem lehet nagyom a jelenlegi dátumnál!";
				}
			}
		
		if($data_error == false) {
				date_default_timezone_set("Europe/Budapest"); 
				$reg_date = date("Y.m.d");
				mb_internal_encoding("UTF-8");
				
				// create regid
				// SELECT * FROM `form_data` WHERE `id` = '2'
				
				// generate id
					$regid = rand(1000000, 9999999) . rand(1000000, 9999999); // notice: make function what checks that id isn't exist in db 
				
				
				
				// get ip
				if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
					$ip = $_SERVER['HTTP_CLIENT_IP'];
				} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
					$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
				} else {
					$ip = $_SERVER['REMOTE_ADDR'];
				}
				
				
				// connect sql - OOP
				$sql = @ NEW MySQLi('localhost','root','','epam_ptsrch');
				if($sql->connect_errno) {
					echo "Adatbázis hiba: ", $sql->connect_errno." ".$sql->connect_error;
					exit();
				}
				$query = "SET NAMES 'utf8'";
				$sql->query($query) or die("Hiba! Az adatbázis nem elérhető. Probáld újra később!");
				$query = "INSERT INTO `form_data` (`pet_species`,`regid`,`pet_variety`,`pet_city`,`pet_date`,`pet_characterization`,`pet_third_choose`,`user_name`,`user_mobile`,`user_mail`,`reg_ip`,`reg_date`) VALUES ('".$pet_species."', '".$regid."', '".$pet_variety."', '".$pet_city."', '".$pet_date."', '".$pet_characterization."', '".$pet_third_choose."', '".$user_name."', '".$user_mobile."', '".$user_mail."', '".$ip."', '".$date_now."')";
				$sql->query($query) or die("Hiba! Az adatbázis nem elérhető. Probáld újra később!");
				echo '<script>setTimeout(function(){ tbswcr_scc(); }, 1);</script>';
				echo '<h1 style="margin: 5px; padding: 5px; color: #666; text-align: center;">Sikeresen regisztráltad a kisállatot!</h1>';
				echo '<h3 style="margin: 5px; padding: 5px; color: #666; text-align: center;">Szívből reméljük, hogy jelentkezni fognak.</h3>';
		}
		
		// error_log
		if($data_error == true) {
			echo '<script>setTimeout(function(){ tbswcr(); }, 1);</script>';
			echo '<div class="error">Hiba:<br/>'. $log_error . '</div>';
		}
	}
 ?>
 	<div id="form-succ-ind">
<form method="post" enctype="multipart/form-data">
		<p style="color: #666; padding: 3px; margin: 4px; margin-left: 10px;">1. Ellenőrizze az adatbázisban, hogy a kisállatot nem-e esetleg megtalálták / a gazda már regisztrálta a megtaláltat mint eltűnt állat!</p>
		<p style="color: #666; padding: 3px; margin: 4px; margin-left: 10px;">2. Ha nem, adja meg a következő adatokat a regisztrációhoz:</p>
		<br/>
		<br/>
		<label for="species"><b>Faj megnevezése*:</b></label><input id="species" name="species" class="sw-input" type="text" placeholder="Faj (Kutya, Macska...)" value="<?php if ($pet_species) {echo $pet_species;} ?>"></input>
		<br/>
		<label for="variety"><b>Állatfajta*:</b></label><input id="variety" name="variety" class="sw-input" type="text" placeholder="Fajta (Beagle, Perzsa...)" value="<?php if ($pet_variety) {echo $pet_variety;} ?>"></input>
		</br>
		<label for="city"><b>Eltűnés/megtalálás városa*:</b></label><input id="city" name="city" class="sw-input" type="text" placeholder="Város (Budapest, Szeged...)" value="<?php if ($pet_city) {echo $pet_city;} ?>">* Budapest esetén kerületet is!</input>
		<br/>
		<label for="date"><b>Eltűnés/megtalálás ideje:</b></label><input id="date" name="date" class="sw-input" type="date" placeholder="" value="<?php if ($pet_date) {echo $pet_date;} ?>"></input>
		<br/>
		<label for="characterization"><b>Leírás*:</b></label><textarea type="text" name="characterization" id="characterization" style="margin: 5px auto; display: block; min-width: 95%; max-width: 95%; min-height: 70px; max-height: 150px;" class="sw-input" placeholder="Leírás - Azt kérjünk öntől, hogy írja le az állat főbb jellemvonásait (Szőrzet tulajdonságai, Szeme színe...). Ideális esetben írja le olyan jellemvonásait ami megkönnyítheti a beazonosítást (Seb, Bicegő járás...)."><?php if ($pet_characterization) {echo $pet_characterization;} ?></textarea>
		<br/>
		<br/>
		<p style="color: #666; padding: 3px; margin: 4px; margin-left: 10px;">3. Válassza ki a megfelelőt!*</p>
		<input type="hidden" id="third_choose" name="third_choose" value="Empty"></input>
		<br/>
		<input onclick="chs_lost();" id="pet_lost" name="pet_third" type="radio"> Elveszett állat</input>
		<br/>
		<br/>
		<input onclick="chs_find();" id="pet_find" name="pet_third" type="radio"> Megtalált állat</input>
		<br/>
		<br/>
		<p style="color: #666; padding: 3px; margin: 4px; margin-left: 10px;">4. Értesítési adatok</p>
		<br/>
		<label for="name"><b>Név*:</b></label><input id="name" name="name" class="sw-input" type="text"></input>
		</br>
		<label for="mobile"><b>Telefonszám:</b></label><input id="mobile" name="mobile" class="sw-input" type="number"></input>
		</br>
		<label for="mail"><b>E-mail*:</b></label><input id="mail" name="mail" class="sw-input" type="text"></input>
		<br/>
		<br/>
		* a mező kitöltése kötelező
		<input id="submit" name="submit" class="btn" type="submit" value="Regisztráció"></input>
</form>
	</div>