<div id="search-panel">
	<form method="POST">
	<label for="pet_variety"><b> Fajta:</b></label><input id="pet_variety" name="pet_variety" class="sw-input" type="text" placeholder="Keresés fajta alapján"></input>
	<label for="srch_city"><b> Település:</b></label><input id="srch_city" name="srch_city" class="sw-input" type="text" placeholder="Keresés település alapján"></input>
	<!-- <b> Rendezés:</b>
	<select class="sw-input" name="srch_order" style="width: 20%;" value="f">
		<option></option>
		<option>Idő szerint csökkenő sorrendbe </option>
		<option>Idő szerint növekvő sorrendbe</option>
	</select> -->
	<input id="src_submit" name="src_submit" class="btn" type="submit" value="Keresés"></input>
	</form>
</div>
<div id="database-data">
	<?php
		
		$srch_parameter = "SELECT * FROM `form_data` ORDER BY `id` DESC";
		
		
		if (isset($_POST["src_submit"])) {
			$pet_variety = $_POST["pet_variety"];
			$srch_city = $_POST["srch_city"];
			// $srch_order = $_POST["srch_order"];
			
			strip_tags($pet_variety);
			strip_tags($srch_city);
			
			if(!$pet_variety && !$srch_city) {
				
			}elseif ($pet_variety && $srch_city) {
				$srch_parameter = "SELECT * FROM  `form_data` WHERE `pet_variety` LIKE '%$pet_variety%' OR `pet_city` LIKE '%$srch_city%' ORDER BY  `id` DESC ";
			}elseif(!$pet_variety && $srch_city) {
				$srch_parameter = "SELECT * FROM  `form_data` WHERE `pet_city` LIKE '%$srch_city%' ORDER BY  `id` DESC ";
			}else {
				$srch_parameter = "SELECT * FROM  `form_data` WHERE `pet_variety` LIKE '%$pet_variety%' ORDER BY  `id` DESC ";
			}
			
			
			/*if($srch_order == "Idő szerint csökkenő sorrendbe") {
				$srch_parameter = "reg_date";
				$order_parameter = "DESC";
				
			}elseif ($srch_order == "Idő szerint növekvő sorrendbe") {
				$srch_parameter = "reg_date";
				$order_parameter = "ASC";
			}else {
				
			}*/
			
		}
		
	   $mysqli = NEW MySQLi('localhost','root','','epam_ptsrch');
	   $query = "SET NAMES 'utf8'";
	   $mysqli->query($query) or die("Hiba! Az adatbázis nem elérhető ezért az adatok nem megjeleníthetők!");
	   $query = "$srch_parameter";
	   $mysqli->query($query) or die("Hiba! Az adatbázis nem elérhető ezért az adatok nem megjeleníthetők!");
	   $resultSet = $mysqli->query($query);
	   if($resultSet->num_rows != 0) {
		while($rows = $resultSet->fetch_assoc()) {
			$display_id = $rows["id"];
			$display_regid = $rows["regid"];
			$display_pet_species = $rows["pet_species"];
			$display_pet_variety = $rows["pet_variety"];
			$display_pet_city = $rows["pet_city"];
			$display_pet_date= $rows["pet_date"];
			$display_pet_characterization = $rows["pet_characterization"];
			$display_pet_third_choose = $rows["pet_third_choose"];
			$display_pet_user_name = $rows["user_name"];
			$display_pet_user_mobile = $rows["user_mobile"];
			$display_pet_user_mail = $rows["user_mail"];
			$display_pet_reg_date = $rows["reg_date"];
			
			if(!$display_pet_user_mobile) {}else{
				$display_pet_user_mobile = "<b>Telefonszám:</b> " . $display_pet_user_mobile;
			}
			
			$display_pet_species = mb_strtolower($display_pet_species);
			
			if (strlen($display_pet_species) > 51) {
				$display_pet_species = substr($display_pet_species,0,50) . "...";
			}
			
			if ($display_pet_third_choose == "1") {
				$typeofdisplay = "Elveszett";
			}else {
				$typeofdisplay = "Megtalált";
			}
				
			echo '
					<div class="data-element">
						<div class="data-element-title">'. $typeofdisplay .' '. $display_pet_species .' keresi gazdáját ('.$display_pet_city.')</div>
						<hr style="margin: 5px 0px;"></hr>
						<div class="data-element-attr"><span style="text-decoration: underline;">Faj:</span> '.$display_pet_species.'</div>
						<div class="data-element-attr"><span style="text-decoration: underline;">Fajta:</span> '.$display_pet_variety.'</div>
						<div class="data-element-attr"><span style="text-decoration: underline;">Megtalálás helye:</span> '.$display_pet_city.'</div>
						<div class="data-element-attr"><span style="text-decoration: underline;">Státusz:</span> '. $typeofdisplay .' '. $display_pet_species .' keresi gazdáját.</div>
						<div class="data-element-attr" style="width: 95%; overflow-x: hidden;"><span style="text-decoration: underline;">Állat leírása:</span> '. $display_pet_characterization .'</div>
						<div class="data-element-attr"><span style="text-decoration: underline;">Elérhetőség:</span> <b>E-mail cím:</b> '.$display_pet_user_mail. " " .$display_pet_user_mobile.'</div>
						<hr style="margin: 5px 0px;"></hr>
						<div><b class="data-element-name">'.$display_pet_user_name.'</b> | <span class="data-element-city">'.$display_pet_city.'</span> | <span class="data-element-date">'.$display_pet_reg_date.'</span></div>
					</div>
			';	
		}
	   }else {
			echo '<h1 style="text-align: center; color: #535253; font-weight: bold; padding: 10px;">Nincs találat!</h1>';
	   }
	?>
	
</div>