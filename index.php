<html>
   <head>
      <?php
         define("WEB_INDEX", "1");
         ?>
		 <!-- Created for Epam summer intenship competition, Epam 2018 - Created by tkSrchD (Tápai Dávid) -->
		  <meta charset="UTF-8">
		  <meta name="description" content="Web application for lost pets. You can create statements about your missed pet, and from the other side people can alert the owners if they found the pet.">
		  <meta name="keywords" content="Petsearch, Epam summer internship">
		  <meta name="author" content="Created for Epam summer internship competition, Epam 2018 - Created by tkSrchD (Tápai Dávid).">
		  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		  <script src='https://code.jquery.com/jquery-3.3.1.min.js'></script>
		  <script src='js/pswitch.js'></script>
		  <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
		  <link rel="stylesheet" type="text/css" href="css/style.css"/>
	</head>
	<body>
		<div id="wrapper">
			<img id="sm-logo" src="pics/smlogo.png"></img>
			<div id="header">
				<div id="logo">
					<img src="pics/logo.png"></img>
				</div>
				<div id="menu">
					<ul>
						<a href="index.php"><li>Főoldal</li></a>
						<a href="index.php?s=about"><li>Az oldalról</li></a>
						<a href="index.php?s=search"><li style="color: #20B3CE;">Állatkereső</li></a>
					</ul>
				</div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
			<div id="container">
				<?php
				 if(isset($_GET["s"]) && $_GET["s"]!="") {
					$s = $_GET["s"];
					if(file_exists("inc/".$s.".php")) {
						include_once ("inc/".$s.".php");
					} else {
						include_once ("inc/404.php");
					}
				}else {
					include_once("inc/main.php");
				}
				?>
			</div>
			<div id="footer">
				<p>Készítette: Tápai Dávid</p>
				<p><i>Epam Nyári Gyakorlat 2018 - Pályamunka</i></p>
			</div>
		</div>
	</body>
</html>